# include <vector>

class Node{
	public :
		std::string name;			 		// node name
		std::vector<std::string> inputs;  	// inputs name
		std::vector<std::string> outputs;  	// outputs name
		int nbOutput, nbInput;				// number of input / output
		Node( char node_name[20] , std::vector<std::string> in  , std::vector<std::string> out , int& nbIn , int& nbOut ) 
			: name(node_name) , nbOutput(nbOut) , nbInput(nbIn) , inputs(in) , outputs(out) {}
};

class NodeHandler{

	std::vector<Node> nodes;

	public : 
		void add ( Node& toAdd ) { nodes.push_back(toAdd); }
		size_t size() { return nodes.size(); }
	 	Node& operator[](std::size_t idx) { return nodes[idx]; }
		const Node& operator[](std::size_t idx) const { return nodes[idx]; }
};