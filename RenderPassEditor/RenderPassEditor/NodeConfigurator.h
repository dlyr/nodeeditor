# include <imgui_internal.h>
# include <algorithm> 
# include <string>
# include <iostream>
# include "NodeHandler.h"

static int in = 1 ; 			// Nb in
static int out = 1 ; 			// Nb out
static char nodeName[20] = "" ; // Node Name
//static int in_selectedItem = 0; 
//static int out_selectedItem = 0;
static NodeHandler nodes;
bool invalid = false;

// 10 entrées sorties identifées par 10 char
char inputs[10][10] {"","","","","","","","","",""};
char outputs[10][10] {"","","","","","","","","",""};

void invalid_node();
void create_node();
void display_parameters();


// Interface de création de noeud
// Utilise les fonctions prototypées ci-dessus
void NodeCreator(){ 

	ImGui::SetWindowSize( ImVec2(576, 680) , 100.f );
	ImGui::Begin(" Node Configurator ", NULL, ImGuiWindowFlags_AlwaysAutoResize);

	if ( ImGui::Button("Create Node") )
		create_node();

	if ( invalid )
		invalid_node();

	ImGui::SameLine();
	ImGui::Button("Delete"); // Pas encore géré 

	 // Slider nombre d'entrée sortie
	ImGui::SliderInt("Nb Input", &in, 1, 10);
	ImGui::SliderInt("Nb Output", &out, 1, 10);

	// Nom du noeud 
	ImGui::InputTextWithHint("Name", "RenderPass Name", nodeName, IM_ARRAYSIZE(nodeName));

	display_parameters();

	ImGui::End();

}


// Affiche les champs de configuration des params
// En fonction des valeurs des sliders qui définissent le nombre d'I/O
void display_parameters(){

	int max = std::max(in,out);
	int tmpIn, tmpOut;
	tmpIn = tmpOut = 0;
	std::string inId = "";
	std::string outId = "";
	ImGui::Columns(2, nullptr, false);
	for ( int i = 0 ; i < max ; i ++ ){

		if ( tmpIn < in  ){
			inId = "In " + std::to_string((tmpIn++)+1);
			//if (ImGui::Selectable( inId.c_str() , i == in_selectedItem))
			  //in_selectedItem = i;
			ImGui::Text("%s",inId.c_str());
			ImGui::SameLine();
			inId = "##" + inId;
			ImGui::InputTextWithHint( inId.c_str(), "Input Name", inputs[tmpIn-1], IM_ARRAYSIZE(inputs[tmpIn-1]) );
		}

		ImGui::NextColumn(); // ImGui::SameLine();
		if ( tmpOut < out  ){
			outId = "Out " + std::to_string((tmpOut++)+1);
			//if (ImGui::Selectable( outId.c_str() , i == out_selectedItem))
			  //out_selectedItem = i;
			ImGui::Text("%s",outId.c_str());
			ImGui::SameLine();
			outId = "##" + outId;
			ImGui::InputTextWithHint( outId.c_str(), "Output Name", outputs[tmpOut-1], IM_ARRAYSIZE(outputs[tmpOut-1]) );
		}
		ImGui::NextColumn();
	}
}


// Affiche un popup d'avertissement 
void invalid_node(){
	ImGui::OpenPopup("Invalid Node");
	if ( ImGui::BeginPopupModal("Invalid Node", &invalid ))
		ImGui::Text(" A node takes at least : \n   -A Name\n   -1 Input\n   -1 Output");
	if (ImGui::Button("Close")){
		invalid = false;
		ImGui::CloseCurrentPopup();
	}
	ImGui::EndPopup();
}

// Copie les informations et créer
// le noeud dans le NodeHandler
void create_node(){
	std::vector<std::string> vec_inputs, vec_outputs;
	int nbIn , nbOut ;
	nbIn = nbOut = 0;

	for ( char * i : inputs )
		if ( strcmp(i,"") != 0 )
		{
			vec_inputs.push_back(i);
			nbIn ++;
			i = (char*) ""; // Remove param for next node
		}

	for ( char * o : outputs )
		if  ( strcmp(o,"") != 0  )
		{
			vec_outputs.push_back(o);
			nbOut++;
			o = (char*) ""; // Remove param for next node
		}

	if ( ( nbIn != 0 ) && ( nbOut != 0 ) && ( strcmp(nodeName,"") != 0 )  )
	{
		invalid = false;
		Node newNode(nodeName, vec_inputs, vec_outputs, nbIn, nbOut);
		nodes.add(newNode);
	}
	else
		invalid  = true;
}